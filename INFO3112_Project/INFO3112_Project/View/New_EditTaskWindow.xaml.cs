﻿using System;
using System.Windows;

namespace INFO3112_Project.View
{
	/// <summary>
	/// Interaction logic for New_EditTaskWindow.xaml
	/// </summary>
	public partial class New_EditTaskWindow : Window
    {
		bool isNewTask;
		int position;
        public New_EditTaskWindow(ProjectTasks editTask, bool isNew, int pos)
        {
            InitializeComponent();
			isNewTask = isNew;
			position = pos;
			LoadComponents(editTask);
        }

        private void LoadComponents(ProjectTasks editTask)
        {
            tbDesc.Text = editTask.description;
			dpStart.SelectedDate = editTask.startDate;
			dpStart.SelectedDate = editTask.finishDate;
			tbEst.Text = editTask.timeEst.ToString();
			if (editTask.owner != null)
			{
				tbOwner.Text = editTask.owner.ToString();
				for (int i = 0; i < editTask.users.Count; ++i)
				{
					if (i != editTask.users.Count - 1)
					{
						tbUsers.Text += editTask.users[i] + ", ";
					}
					else
					{
						tbUsers.Text += editTask.users[i];
					}
				}
			}
			else
			{
				tbOwner.Text = "";
				tbUsers.Text = "";
			}
			tbAct.Text = editTask.timeAct.ToString();

		}

		private void BtnSave_Click(object sender, RoutedEventArgs e)
		{
			ProjectTasks pt = new ProjectTasks();
			pt.description = tbDesc.Text;
			pt.startDate = dpStart.SelectedDate.Value.Date;
			pt.finishDate = dpEnd.SelectedDate.Value.Date;
			pt.timeEst = Int32.Parse(tbEst.Text);
			pt.owner = tbOwner.Text;
			pt.name = tbUsers.Text;
			if (isNewTask)
			{
				((MainWindow)this.Owner).NewTask(pt);
			}
			else
			{
				((MainWindow)this.Owner).EditTask(pt, position);
			}
			Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
