﻿using System.Windows;

namespace INFO3112_Project.View
{
	/// <summary>
	/// Interaction logic for Analytics.xaml
	/// </summary>
	public partial class Analytics : Window
	{
		public Analytics(ProjectTasks tasks)
		{
			InitializeComponent();
			AnalyseData(tasks);
		}

		private void AnalyseData(ProjectTasks pt) {
			tbDifference.Text = (pt.timeEst - pt.timeAct).ToString();
			if (pt.finishDate != null)
			{
				tbComplete.Text = "100%";
			}
			else
			{
				tbComplete.Text = "0%";
			}
			tbRecalculation.Text = "";
		}

		private void BtnCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
