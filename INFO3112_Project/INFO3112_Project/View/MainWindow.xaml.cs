﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace INFO3112_Project
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>

	public class ProjectTasks
    {
        //Attributes
        public string name { get; set; }
        public string description { get; set; }
        public int timeEst { get; set; }
        public double timeAct { get; set; }
        public string owner { get; set; }
        public DateTime startDate { get; set; }
        public DateTime finishDate { get; set; }
        public List<string> users { get; set; }

        //Methods
        public override string ToString()
        {
            return description;
        }
    }

    public class Users
    {
        //Attributes
        public string name { get; set; }
        public string role { get; set; }
    }

    public class JSONController
    {
        //Attributes
        private string json_path = @".\..\..\projects.json";
        public ObservableCollection<ProjectTasks> myTasks { get; set; }

        //Methods 
        //Get collection
        //Open the JSON and store data in array of tasks.
        public ObservableCollection<ProjectTasks> getAll()
        {
            myTasks = new ObservableCollection<ProjectTasks>();
            //Open JSON
            using (StreamReader r = new StreamReader(json_path))
            {
                string json = r.ReadToEnd();
                myTasks = JsonConvert.DeserializeObject<ObservableCollection<ProjectTasks>>(json);
            }

            return myTasks;
        }
        //Save JSON function
        public void saveJson(List<ProjectTasks> taskList)
        {
            try
            {
                File.WriteAllText(json_path, JsonConvert.SerializeObject(taskList));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex);
            }
        }
    }
    public partial class MainWindow : Window
    {
        public ObservableCollection<ProjectTasks> tasks;
        ProjectTasks newTask = new ProjectTasks();

        public MainWindow()
        {
            InitializeComponent();

            //JSON
            JSONController con = new JSONController();
            tasks = new ObservableCollection<ProjectTasks>();
            tasks = con.getAll();
            DataContext = tasks;
        }

		public void NewTask(ProjectTasks pt)
		{
				tasks.Add(pt);
		}

		public void EditTask(ProjectTasks pt, int pos)
		{
			tasks[pos].description = pt.description;
			tasks[pos].owner = pt.owner;
			tasks[pos].name = pt.name;
			tasks[pos].startDate = pt.startDate;
			tasks[pos].finishDate = pt.finishDate;
			tasks[pos].timeEst = pt.timeEst;
			tasks[pos].timeAct = pt.timeAct;
		}

		private void CommandBinding_Save(object sender, ExecutedRoutedEventArgs e)
        {
            JSONController con = new JSONController();
            List<ProjectTasks> myTasks = new List<ProjectTasks>(tasks);
            con.saveJson(myTasks);
            MessageBox.Show("Tasks saved to file!");
        }

        private void CommandBinding_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void OnClick_NewTask(object sender, RoutedEventArgs e)
        {
            newTask = new ProjectTasks();
            View.New_EditTaskWindow NewWindow = new View.New_EditTaskWindow(newTask, true, 0);
			NewWindow.Owner = this;
            NewWindow.ShowDialog();
        }

        private void OnClick_EditTask(object sender, RoutedEventArgs e)
        {
			int pos = 0;
            //Get selected item.
            ProjectTasks selectedTask = (ProjectTasks)lbTasks.SelectedItem;
			//Send item to edit window
			for (int i = 0; i < tasks.Count; ++i)
			{
				if (selectedTask == tasks[i])
					pos = i;
			}
            View.New_EditTaskWindow NewWindow = new View.New_EditTaskWindow(selectedTask, false, pos);
			NewWindow.Owner = this;
			NewWindow.ShowDialog();
        }

		private void OnClick_NewAnalytics(object sender, RoutedEventArgs e)
		{
			ProjectTasks selectedTask = (ProjectTasks)lbTasks.SelectedItem;
			View.Analytics NewAnalytics = new View.Analytics(selectedTask);
			NewAnalytics.Owner = this;
			NewAnalytics.ShowDialog();
		}

        private void OnClick_About(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("INFO3112 - Main Project" +
                "\nGroup 19 - \"The Love Debuggers\"" +
                "\n\nBrian Cowan" +
                "\nJames Erb-Ohnsorge" +
                "\nRaymond Beaune");
        }
    }
}
